Name:		less
Version:	668
Release:        1
Summary:	Less is a pager that displays text files.
License:	GPL-3.0-only and BSD-2-Clause
URL:		https://www.greenwoodsoftware.com/less
Source0:	https://www.greenwoodsoftware.com/less/%{name}-%{version}.tar.gz
Source1:        lesspipe.sh
Source2:        less.sh
Source3:        less.csh
Patch0:         less-394-time.patch
Patch1:         less-475-fsync.patch

BuildRequires:	gcc make ncurses-devel autoconf automake libtool

%description
Less is a pager. A pager is a program that displays text files.
Other pagers commonly in use are more and pg. Pagers are often
used in command-line environments like the Unix shell and the MS-DOS
command prompt to display files.

Less is not an editor. You can't change the contents of the file
you're viewing. Less is not a windowing system. It doesn't have
fancy scroll bars or other GUI (graphical user interface) elements.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
rm -f ./configure
autoreconf -ivf
%configure
%make_build CFLAGS="%{optflags} -D_GNU_SOURCE -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64"

%install
%make_install
mkdir -p %{buildroot}%{_sysconfdir}/profile.d
install -p        %{S:1} %{buildroot}%{_bindir}
install -p -m 644 %{S:2} %{buildroot}%{_sysconfdir}/profile.d
install -p -m 644 %{S:3} %{buildroot}%{_sysconfdir}/profile.d

%files
%license LICENSE COPYING
%{_bindir}/less*
%{_sysconfdir}/profile.d/*

%files help
%doc README NEWS INSTALL
%{_mandir}/man1/*

%changelog
* Fri Oct 18 2024 Funda Wang <fundawang@yeah.net> - 668-1
- update to 668

* Wed Aug 14 2024 Funda Wang <fundawang@yeah.net> - 661-2
- do not use bashisms in lesspipe.sh

* Sun Jul 14 2024 Funda Wang <fundawang@yeah.net> - 661-1
- Update to 661
- add lesspipe as most other distros do

* Sat May 11 2024 zhangxingrong <zhangxingrong@uniontech.com> - 633-4
- backport some upstream patch

* Mon Apr 29 2024 huyubiao <huyubiao@huawei.com> - 633-3
- fix problem when a program piping into less reads from the tty, like sudo asking for password

* Mon Apr 22 2024 wangjiang <wangjiang37@h-partners.com> - 633-2
- fix CVE-2024-32487

* Tue Jan 30 2024 hongjinghao <hongjinghao@huawei.com> - 633-1
- Update to 633
 
* Thu Mar 16 2023 EibzChan <chenbingzhao@huawei.com> - 608-4
- remove unstable test patches and test compilation option

* Fri Feb 17 2023 hongjinghao <hongjinghao@huawei.com> - 608-3
- fix CVE-2022-46663

* Thu Dec 15 2022 EibzChan <chenbingzhao@huawei.com> - 608-2
- Type:test enhancement
- ID:NA
- SUG:NA
- DESC:backport patches from upstream to enable make check.
       backport-makecheck-0000-add-lesstest.patch
       backport-makecheck-0001-Work-on-lesstest-remove-rstat-add-LESS_DUMP_CHAR.patch
       backport-makecheck-0002-lesstest-correctly-handle-less-exit-during-run_inter.patch
       backport-makecheck-0003-Some-runtest-tweaks.patch
       backport-makecheck-0004-Rearrange-signal-handling-a-little.patch
       backport-makecheck-0005-Don-t-setup_term-in-test-mode.patch
       backport-makecheck-0006-Compile-fixes.patch
       backport-makecheck-0007-Pass-less-specific-env-variables-to-lesstest-get-rid.patch
       backport-makecheck-0008-Move-terminal-init-deinit-to-run_interactive-since.patch
       backport-makecheck-0009-Fix-bug-in-setting-env-vars-from-lt-file-in-test-mod.patch
       backport-makecheck-0010-Make-runtest-work.patch
       backport-makecheck-0011-lesstest-in-interactive-mode-call-setup_term-before-.patch
       backport-makecheck-0012-lesstest-log-LESS_TERMCAP_-vars-so-termcap-keys-etc.patch
       backport-makecheck-0013-lesstest-accommodate-stupid-termcap-design-where-the.patch
       backport-makecheck-0014-lesstest-maketest-should-not-overwrite-existing-lt-f.patch
       backport-makecheck-0015-lesstest-add-O-option-to-maketest-if-textfile-is-not.patch
       backport-makecheck-0016-lesstest-add-O-option-to-lesstest.patch
       backport-makecheck-0017-lesstest-handle-colored-text-with-less-R.patch
       backport-makecheck-0018-lesstest-add-e-option.patch
       backport-makecheck-0019-lesstest-split-display_screen-into-display_screen_de.patch
       backport-makecheck-0020-Add-E-option.patch
       backport-makecheck-0021-Consistent-style.patch
       backport-makecheck-0022-Obsolete-file.patch
       backport-makecheck-0023-Tuesday-style.patch
       backport-makecheck-0024-Tuesday-style.patch
       backport-makecheck-0025-lesstest-add-support-for-combining-and-composing-cha.patch
       backport-makecheck-0026-Minor-runtest-output-tweaks.patch
       backport-makecheck-0027-lesstest-lt_screen-should-clear-param-stack-after-pr.patch
       backport-makecheck-0028-Handle-fg-and-bg-colors.patch
       backport-makecheck-0029-Have-lt_screen-use-ANSI-sequences-for-bold-underline.patch
       backport-makecheck-0030-ESC-m-should-clear-attributes-as-well-as-colors.patch
       backport-makecheck-0031-lesskey-make-lt_screen-treat-ESC-0m-like-ESC-m.patch
       backport-makecheck-0032-Store-2-char-hex-values-in-log-file-rather-than-bina.patch
       backport-makecheck-0033-lesstest-Make-display_screen_debug-write-to-stderr-n.patch
       backport-makecheck-0034-lesstest-Clear-screen-at-end-of-maketest-in-case-ter.patch
       backport-makecheck-0035-lesstest-Verify-that-the-less-binary-is-built-with-D.patch
       backport-makecheck-0036-Add-check-target-to-Makefile-to-run-lesstest.patch
       backport-makecheck-0037-Don-t-set-LESS_TERMCAP_xx-environment-vars-from-term.patch
       backport-makecheck-0038-lesstest-Remove-unnecessary-exit_all_modes-field-fro.patch
       backport-makecheck-0039-lesstest-Add-some-initial-lt-files.patch
       backport-makecheck-0040-lesstest-Remove-empty-lt-file.patch
       backport-makecheck-0041-lesstest-Add-a-couple-more-lt-files.patch
       backport-makecheck-0042-Make-make-check-work-regardless-of-directory-where-l.patch

* Fri Nov 18 2022 dillon chen <dillon.chen@gmail.com> - 608-1
- update to 608

* Thu Oct 13 2022 fuanan <fuanan3@h-partners.com> - 590-2
- DESC:fix the changelog exception macro

* Fri Sep 24 2021 fuanan <fuanan3@huawei.com> - 590-1
- update version to 590

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 563-3
- DESC: delete -S git from autosetup, and delete BuildRequires git

* Fri May 28 2021 fuanan <fuanan3@huawei.com> - 563-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:[add] backport patches from upstream
       Create only one ifile when a file is opened under different names.
       Remove extraneous frees, associated with removed call to lrealpath.
       Fix crash when call set_ifilename with a pointer to the name that is
       Remove unnecessary call to pshift in pappend.
       Reset horizontal shift when opening a new file.
       Protect from buffer overrun.
       Make histpattern return negative value to indicate error.
       Lesskey: don't translate ctrl-K in an EXTRA string.
       Ignore SIGTSTP in secure mode.
       Fix "Tag not found" error while looking for a tag's location
       Fix minor memory leak with input preprocessor.

* Thu Jan 21 2021 wangchen <wangchen137@huawei.com> - 563-1
- Update to 563

* Thu Jan 09 2020 openEuler Buildteam <buildteam@openeuler.org> - 551-3
- Delete unneeded files

* Fri Sep 27 2019 yefei <yefei25@huawei.com> - 551-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: delete irrelevant comment

* Tue Sep 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 551-1
- Package Init
